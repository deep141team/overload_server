// =================================================================
// get the packages we need ========================================
// =================================================================
var http = require('http');
var app		 	= require('express')();

var main        = require('./app/sockets')(app);
var ioServer 	= main.server;

var bodyParser  = require('body-parser');
var morgan      = require('morgan');
var mongoose    = require('mongoose');

var config 		= require('./config'); // get our config file

var apiRoutes 	= require('./app/api');
var web 		= require('./app/web');

var cors = require('cors')

var _ = require('underscore');


var multipart = require('connect-multiparty');

app.use(multipart({ uploadDir: './tmp' }))

app.use(cors());
// =================================================================
// configuration ===================================================
// =================================================================
var port = process.env.PORT || 3000; // used to create, sign, and verify tokens
// mongoose.connect(config.database, {useMongoClient: true}); // connect to database
app.set('superSecret', config.secret); // secret variable

app.set('socket_method', main.socket);

// use body parser so we can get info from POST and/or URL parameters
app.use(bodyParser({limit: '20mb'}))
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// use morgan to log requests to the console
app.use(morgan('dev'));

// Add headers
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', false);

    // Pass to next layer of middleware
    next();
});

app.use(web(app, port));

app.use('/api', apiRoutes(app));

// =================================================================
// start the server ================================================
// =================================================================
ioServer.listen(port)

console.log('Magic happens at http://localhost:' + port);
