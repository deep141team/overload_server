'use strict';

var Mongoose 	= require('mongoose');

var UserSchema = new Mongoose.Schema({
    email: { type: String, required: true },
    username: { type: String, required: true },
    password: { type: String, default: null },
    rooms: [ { type: Array, default: [] } ],
    photo: {type: String, default: ''},
    external_id: {type: String, default: ''}
},{ 
    usePushEach: true 
});

// Create a user model
var userModel = Mongoose.model('user', UserSchema);

module.exports = userModel;