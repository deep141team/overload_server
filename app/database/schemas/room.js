'use strict';

var mongoose  = require('mongoose');


var RoomSchema = new mongoose.Schema({
	// user_id1:user_id2
	room_id: {type:String},
    title: { type: String, default: '' },
    connections: [],
    private: { type: Boolean, default: true },
    owner: {type:String, default: ''},
    messages: [ { 
    	id: mongoose.Schema.ObjectId,
		from: String,
    	text: String,
    	sent_ts: Date,
    	read_by: [],
        delivered_to: [],
        // type in [string, image, file]
        message_type: {type:String, default: 'string'},
        file_name: String,
        file_path: String
    }]
},{ 
    usePushEach: true 
});

var roomModel = mongoose.model('room', RoomSchema);

module.exports = roomModel;