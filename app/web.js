var express 	= require('express');
var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
var User   = require('./models/user'); // get our mongoose model
var path = require('path');
var fs = require('fs');
var bcrypt = require('bcryptjs');
const util = require('util');
const rename = util.promisify(fs.rename);
const mkdirSync = function (dirPath) {
  try {
    fs.mkdirSync(dirPath)
  } catch (err) {
    // if (err.code !== 'EEXIST') throw err
  }
  return dirPath;
}


// ---------------------------------------------------------
// get an instance of the router for api routes
// ---------------------------------------------------------
var webRoutes = express.Router(); 

module.exports = function(app, port) {
	var config = {
		jwt_life: 86400,
		jwt_secret: app.get('superSecret')
	}
	// =================================================================
	// routes ==========================================================
	// =================================================================

	webRoutes.post('/login', function(req, res) {

		// find the user
		User.findOne({
			email: req.body.email
		}, function(err, user) {

			if (err) throw err;

			if (!user) {
				return res.json({ success: false, message: 'Authentication failed. User not found.' });
			} 
			if (user) {
				
				if (!bcrypt.compareSync(req.body.password, user.password)) {
					return res.json({ success: false, message: 'Authentication failed. Wrong password.' });
				}
				
				// create a token
				var payload = {
					id: user._id	
				}
				var token = jwt.sign(payload, config.jwt_secret, {
					expiresIn: config.jwt_life // expires in 24 hours
				});

				res.json({
					success: true,
					message: 'Enjoy your token!',
					token: token,
					user: {
						username: user.username,
						email: user.email,
						id: user._id,
						photo: user.photo
					}
				});
			}
		});
	});

	webRoutes.post('/register', function(req, res) {
		// find the user
		User.findOne({
			email: req.body.email
		}, function(err, user) {
			if (err) throw err;

			if (user) {
				return res.json({ success: false, message: 'Email already used' });	
			}

			if (!req.body.password || !req.body.confirm_password || !req.body.email || !req.body.username) {
				res.json({ success: false, message: 'All fields are required!' });
			}

			if (req.body.password != req.body.confirm_password) {
				res.json({ success: false, message: 'Passwords dont match.' });
			}

			var salt = bcrypt.genSaltSync(10);
			var hash = bcrypt.hashSync(req.body.password, salt);

			// create a sample user
			var nick = new User({ 
				username: req.body.username, 
				password: hash,
				email: req.body.email,
				photo: '',
				external_id: req.body.user_id
			});
			nick.save(function(err, user) {
				if (err) throw err;


				if (req.files && req.files.file) {
					var file = req.files.file;
					var file_name = user._id+file.name.split('.').pop();
					var file_path = mkdirSync(mkdirSync(path.join(__dirname, '../uploads'))+'/shared')+'/' +file_name;

					rename(file.path, file_path).then(() => {
						req.app.get('socket_method').notifyRegister(user._id);

						User.findOneAndUpdate({_id: user._id}, {$set:{photo: file_name}}, function(err, doc){
						    if(err){
						        console.log("Something wrong when updating data!");
						    }
							res.json({
								success: true
							});
						});
					})
				} else {
					req.app.get('socket_method').notifyRegister(user._id);
					res.json({
						success: true
					});
				}

			});
		});
	});

	return webRoutes;
};