'use strict';

var config 	= require('../config');
var redis 	= require('redis').createClient;
var adapter = require('socket.io-redis');

var Room = require('./models/room');
var User = require('./models/user');

var mongoose = require('mongoose'); 
var _ = require('underscore');


/*
	socket 	: socket,
	user_id : data.user_id,
	token 	: data.token,
	status 	: 'connected'
*/
global.clients = {};



var Rooms = function(socket) {


	Room.find({
		connections: { "$in" : [socket.user_id]},
		private: false
	}, function(err, rooms) {
		if (err) throw err;

		rooms = _.map(rooms, function(room) {
			console.log('groups: mes', room.messages);
			return {
				room_id: room.room_id,
				private: room.private,
				title: room.title,
				messages: room.messages,
				owner: room.owner
			}
		});

		socket.emit('GET-LIST:groups', rooms);
	});
	/*
		data:
			id 			: user_id,
			connections : [this.user.id, user_id],
			room 		: room_id
	*/
	socket.on('GET-LIST:group:messages', function(data) {
		Room.findOne({
			room_id: data.room
		}, function(err, room) {
			if (err) throw err;

			let messages = [];
			
			if (room) {
				let shouldUpdate = false;
				messages = room.messages;
				if (messages.length > 0) {
					messages = _.map(messages, function(m) {
						if (m.delivered_to.indexOf(socket.user_id) == -1) {
							m.delivered_to.push(socket.user_id);
							shouldUpdate = true;
						}
						return m;
					});
					if(shouldUpdate) {
						room.messages = messages;
						room.save(function(err, m) {
							socket.broadcast.emit('GET-LIST:group:messages', {
								room_id: data.room,
								messages: messages,
								room_title: room ? room.title : ''
							});
						})
					}
				}
			}

			socket.emit('GET-LIST:group:messages', {
				room_id: data.room,
				messages: messages,
				room_title: room ? room.title : ''
			});
		});
	});
	socket.on('GET-LIST:group:delete', function(room_id) {
		console.log("DELETE HERE");
		Room.findOne({
			room_id: room_id
		}, function(err, room) {
			if (err) throw err;
			if (socket.user_id == room.owner) {
				// delete

				console.log('delete', room);

				let connections = room_id.split(':');

				room.remove(function(err, r) {

					_.each(connections, function(connection) {
						if (global.clients[connection]) {
							global.clients[connection].socket.emit('GET-LIST:group:delete', {
								room_id: room.room_id,
							});
						}
					})

				});
			}
		});
	});

	/*
		data:
			room_id
			old_room_id
	*/
	socket.on('GET-LIST:group:edit', function(data) {
		Room.findOne({room_id: data.old_room_id}, function(err, room) {
			if(err) throw err;

			if (!room) {
				return;
			}

			let connections = data.room_id.split(':');

			room.connections = connections;
			room.room_id = data.room_id;

			connections = connections.concat(data.old_room_id.split(':')).filter((value, index, self) => self.indexOf(value) === index)

			room.save(function(err, room) {
				if(err) throw err;

				if(room) {
					_.each(connections, function(connection) {
						if (global.clients[connection]) {
							
							Room.find({
								connections: { "$in" : [connection]},
								private: false
							}, function(err, rooms) {
								if (err) throw err;

								rooms = _.map(rooms, function(room) {
									return {
										room_id: room.room_id,
										private: room.private,
										title: room.title,
										messages: room.messages,
										owner: room.owner
									}
								});

								global.clients[connection].socket.emit('GET-LIST:groups', rooms);
							})
						}
					})
				}
			});
		});
	})
	/*
		data:
			id
			title 
	*/
	socket.on('NEW:group', function(data) {
		Room.findOne({room_id: data.id}, function(err, room) {
			if(err) throw err;

			if (room) {
				return;
			}

			let connections = data.id.split(':');

			room = new Room({
				room_id 	: data.id,
			    connections : connections,
			    messages 	: [],
			    private		: connections.length == 2,
			    title 		: data.title ? data.title : 'Room with no title'
			});

			if (connections.length > 2) {
				room.owner = socket.user_id;
			}

			room.save(function(err, room) {
				if(err) throw err;

				if(room) {
					_.each(connections, function(connection) {
						if (global.clients[connection]) {
							global.clients[connection].socket.emit('GET-LIST:groups', [{
								room_id: room.room_id,
								private: room.private,
								title: room.title,
								messages: room.messages,
								owner: room.owner
							}]);
						}
					})
				}
			});
		});
	});

	socket.on('GET-LIST:group:typing:start', function(room_id) {
		let connections = room_id.split(':');

		_.each(connections, function(el) {
			if (global.clients[el]) {
				if (el == socket.user_id) return;
				
				global.clients[el].socket.emit('GET-LIST:group:typing:start', {
					room_id: room_id,
					author: socket.user_id
				});
			}
		})
	})

	socket.on('GET-LIST:group:typing:stop', function(room_id) {
		let connections = room_id.split(':');

		_.each(connections, function(el) {
			if (global.clients[el]) {
				if (el == socket.user_id) return;
				
				global.clients[el].socket.emit('GET-LIST:group:typing:stop', {
					room_id: room_id,
					author: socket.user_id
				});
			}
		})
	})

	/*
	    data:
	      room_id   : room_id,
	      messages  : [ids],
  	*/
	socket.on('GET-LIST:group:messages:read', function(res) {
		Room.findOne({room_id: res.room_id}, function(err, room) {
			if (err) throw err;

			room.messages = _.map(room.messages, function(m) {
				if (res.messages.indexOf(m.id.toString()) > -1 && m.read_by.indexOf(socket.user_id) == -1) {
					m.read_by.push(socket.user_id);
				}
				return m;
			}); 
		
			room.save(function(err, r) {
				// broacast the messages list

				let connections = res.room_id.split(':');

				_.each(connections, function(el) {
					if (global.clients[el]) {
						global.clients[el].socket.emit('GET-LIST:group:messages', {
							room_id: res.room_id,
							messages: room.messages,
							room_title: room ? room.title : ''
						});
					}
				})

				room.messages = _.filter(room.messages, function(m) {
					return m.read_by.length < connections.length;
				});

				room.save(function(err, r) {
					if(err) console.log('GET-LIST:group:messages:read', err);
					
				});
			});
		});
	});
	/*
	    data:
	      room   : room_id,
	      from   : this.user.id,
	      text   : '',
  	*/
	socket.on('NEW:group:message', function(data) {

		Room.findOne({room_id: data.room}, function(err, room) {
			if (err) throw err;
			
			let new_room = false;

			let message = {
				id: mongoose.Types.ObjectId(),
				from: data.from,
		    	text: data.text,
		    	sent_ts: new Date(),
		    	read_by: [data.from],
		    	delivered_to: [data.from],
		    	message_type: 'string'
			};

			let connections = data.room.split(':');

			_.each(connections, function(el) {
				if (global.clients[el] && message.delivered_to.indexOf(el) == -1) {
					message.delivered_to.push(el);
				}
			});

			if (!room) {
				room = new Room({
					room_id 	: data.room,
				    connections : connections,
				    messages 	: [message],
				    private		: connections.length == 2,
				    title 		: data.room_title ? data.room_title : ''
				});

				if (connections.length > 2) {
					room.owner = socket.user_id;
				}
				new_room = true;
			} else {
				room.messages.push(message);
			}

			room.save(function(err, room) {
				if(err) throw err;

				_.each(connections, function(el) {
					if (global.clients[el]) {

						if(new_room && room.private == false) {
							global.clients[el].socket.emit('GET-LIST:groups', [{
								room_id: room.room_id,
								private: room.private,
								title: room.title,
								messages: room.messages,
								owner: room.owner
							}]);
						} else {
							global.clients[el].socket.emit('NEW:group:message', {
								room_id: room.room_id,
								message: message
							});
						}
					}
				})
			})
		});
	});
};

var ioEvents = function(io) {

	io.on('connection', function(socket) {
		console.log('New user connected.');
	});

	// Users namespace
	io.of('/users').on('connection', function(socket) {

		/**
		**	USERS
		**/
			// save the socket, user_id, token
			socket.on('login', function(data) {
				socket.user_id = data.user_id;

				global.clients[data.user_id] = {
					socket 	: socket,
					user_id : data.user_id,
					token 	: data.token,
					status 	: 'connected'
				};

				User.find({
					_id: data.user_id
				}, function(err, u) {

					User.find({}, function(err, users) {
						if(err) throw err;

						socket.emit("GET-LIST:users", _.map(users, function(user) {
							return {
								id: user._id,
								username: user.username,
								status: global.clients[user._id] ? global.clients[user._id].status : 'disconnected',
								photo: user.photo
							};
						}));

						socket.broadcast.emit('UPDATE:user', {
							update: {
								status: global.clients[data.user_id].status,
								username: u.username,
								id: u._id,
								photo: u.photo
							},
							user_id: socket.user_id
						});
					});
				});

				// register rooms events
				Rooms(socket);
			});

			socket.on('disconnect', function() {
				delete global.clients[socket.user_id];

				socket.broadcast.emit('UPDATE:user', {
					update: {
						status: 'disconnected'
					},
					user_id: socket.user_id
				});
			});
	});

	return {
		notifyRegister: (user_id) => {

			User.find({
				_id: user_id
			}, function(err, u) {

				User.find({}, function(err, users) {
					if(err) throw err;

					var users = _.map(users, function(user) {
						return {
							id: user._id,
							username: user.username,
							status: global.clients[user._id] ? global.clients[user._id].status : 'disconnected',
							photo: user.photo
						};
					});

					_.each(Object.values(global.clients), function(el) {
						el.socket.emit('GET-LIST:users', users);
					})
				});
			});

		},
		// data: {name, path, type}
		upload: (room_id, uploader, data) => {

			Room.findOne({room_id: room_id}, function(err, room) {
				if (err) throw err;
				
				let new_room = false;

				let message = {
					id: mongoose.Types.ObjectId(),
					from: uploader,
			    	text: '',
			    	sent_ts: new Date(),
			    	read_by: [uploader],
			    	delivered_to: [uploader],
			    	message_type: data.type,
			    	file_name: data.name,
        			file_path: data.path
				};

				let connections = room_id.split(':');

				if (!room) {
					room = new Room({
						room_id 	: room_id,
					    connections : connections,
					    messages 	: [message],
					    private		: connections.length == 2,
					    title 		: data.room_title ? data.room_title : ''
					});

					if (connections.length > 2) {
						room.owner = uploader;
					}
					new_room = true;
				} else {
					room.messages.push(message);
				}

				room.save(function(err, room) {
					if(err) throw err;


					_.each(connections, function(el) {
						if (global.clients[el]) {
							if(new_room && room.private == false) {
								global.clients[el].socket.emit('GET-LIST:groups', [{
									room_id: room.room_id,
									private: room.private,
									title: room.title,
									messages: room.messages,
									owner: room.owner
								}]);
							} else {
								global.clients[el].socket.emit('NEW:group:message', {
									room_id: room.room_id,
									message: message
								});
							}
						}
					})
				})
			});
		}
	}
}

/**
 * Initialize Socket.io
 * Uses Redis as Adapter for Socket.io
 *
 */
var init = function(app){

	var server 	= require('http').Server(app);
	var io 		= require('socket.io')(server);

	// Force Socket.io to ONLY use "websockets"; No Long Polling.
	// io.set('transports', ['websocket']);

	// Using Redis
	// let port = config.redis.port;
	// let host = config.redis.host;
	// let password = config.redis.password;

	// let pubClient = redis(port, host, { auth_pass: password });
	// let subClient = redis(port, host, { auth_pass: password, return_buffers: true, });

	// io.adapter(adapter({ pubClient, subClient }));

	// Allow sockets to access session data
	io.use((socket, next) => {
		next();
		// require('../session')(socket.request, {}, next);
	});

	// Define all Events
	;

	// The server object will be then used to list to a port number
	return {
		server,

		socket: ioEvents(io),
	};
}

module.exports = init;