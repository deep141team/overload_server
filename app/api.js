var express 	= require('express');
var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
var User   = require('./models/user'); // get our mongoose model

var _ = require('underscore');

var path = require('path');
var fs = require('fs');

const util = require('util');

// ---------------------------------------------------------
// get an instance of the router for api routes
// ---------------------------------------------------------
var apiRoutes = express.Router(); 

module.exports = function(app) {
	
	function getExtension(filename) {
	    return filename.split('.').pop().toLowerCase();
	}


	function getFileType(filename) {
		var extension = getExtension(filename);


		var images = ["bmp","jpg","png","gif", "jpeg","tiff"];
		if (images.indexOf(extension) > -1) {
			return 'image';
		}
		var audio = ['mp3','wav','aac','amr','ogg','oga','mogg','m4b'];
		if (audio.indexOf(extension) > -1) {
			return 'audio';
		}
		var video = ['mp4','flv','mkv','3gp','ogv','mpg','wmv','avi','3gp'];
		if (video.indexOf(extension) > -1) {
			return 'video';
		}

		var documents = [ "txt","doc","docx","ppt","pptx","pdf","rtf","xls","xlsx"];
		if (documents.indexOf(extension) > -1) {
			return 'document';
		}
		return 'file';
	}

	const mkdirSync = function (dirPath) {
	  try {
	    fs.mkdirSync(dirPath)
	  } catch (err) {
	    // if (err.code !== 'EEXIST') throw err
	  }
	  return dirPath;
	}


	const rename = util.promisify(fs.rename);
	const readFile = util.promisify(fs.readFile); 

	// ---------------------------------------------------------
	// route middleware to authenticate and check token
	// ---------------------------------------------------------
	apiRoutes.use(function(req, res, next) {

		// check header or url parameters or post parameters for token
		var token = req.body.token || req.param('token') || req.headers['x-access-token'];

		// decode token
		if (token) {

			// verifies secret and checks exp
			jwt.verify(token, app.get('superSecret'), function(err, decoded) {			
				if (err) {
					return res.json({ success: false, message: 'Failed to authenticate token.' });		
				} else {
					// if everything is good, save to request for use in other routes
					req.decoded = decoded;	

					next();
				}
			});

		} else {

			// if there is no token
			// return an error
			return res.status(403).send({ 
				success: false, 
				message: 'No token provided.'
			});
			
		}
		
	});

	apiRoutes.post('/upload', function(req, res) {
		if (!req.files || !req.body)
			return res.status(400).json({
				success: false,
				message: 'No files were uploaded.'
			});

		var users = req.body.room_id.split(':');

		var base_folder = mkdirSync(path.join(__dirname, '../uploads'));

		var files = [];
		var files_used = {};

		for (var i = req.files.file.length - 1; i >= 0; i--) {
			if (!files_used[req.files.file[i].originalFileName]) {
				files.push(req.files.file[i]);
				files_used[req.files.file[i].originalFileName] = true;
			} 
		}
		var errs = false;

		_.each(files, function(file) {
			if (getFileType(file.name) == 'file') {
				errs = true;
			}
		});

		if (errs) {
			return res.status(400).json({
				success: false,
				message: 'Some files are unsuported'
			});
		}

		_.each(files, function(file) {
			var file_path = ((new Date()).getTime()) + '.' + getExtension(file.name);

			var new_name= path.join(base_folder, file_path);

			rename(file.path, new_name)
			.then(() => {
				readFile(new_name)
				.then((aux_file) => {
					_.each(users, function(user) {
						var new_path = mkdirSync(base_folder+'/'+user)+'/';
						fs.writeFileSync(path.join(new_path,file_path), aux_file); 
					});

					req.app.get('socket_method').upload(req.body.room_id, req.decoded.id, {
						name: file.name,
						path: file_path,
						type: getFileType(file.name)
					});
				});
			})
		})

		res.send('File uploaded!');
	});

	apiRoutes.get( '/files/:filename', function( req, res ) {
		fs.exists(path.join( __dirname, '../uploads/shared/', req.params.filename),function(exists){
			if (exists) {
				res.sendFile( path.join( __dirname, '../uploads/shared/', req.params.filename ));		
			} else {
				res.sendFile( path.join( __dirname, '../uploads/'+req.decoded.id, req.params.filename ));
			}
		});
	});


	return apiRoutes;
};